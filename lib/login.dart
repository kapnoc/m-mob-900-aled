import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:garden_go/app_localizations.dart';
import 'package:requests/requests.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).translate('Login')),
        ),
        body: Center(
          child: LoginForm(),
        ),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            validator: (String value) {
              if (value.isEmpty) {
                return 'Username field cannot be empty';
              }
              return null;
            },
            decoration: const InputDecoration(hintText: 'Username'),
            controller: usernameController,
          ),
          TextFormField(
            validator: (String value) {
              if (value.isEmpty) {
                return 'Password field cannot be empty';
              }
              return null;
            },
            decoration: const InputDecoration(hintText: 'Password'),
            obscureText: true,
            controller: passwordController,
          ),
          RaisedButton(
            onPressed: () {
              if (_formKey.currentState.validate()) {
                Scaffold.of(context).showSnackBar(
                    const SnackBar(content: Text('Connecting ...')));
                final Map<String, String> body = <String, String>{
                  'username': usernameController.text,
                  'password': passwordController.text,
                };
                Requests.post(
                        'https://aqueous-tundra-22992.herokuapp.com/users/login/',
                        body: body)
                    .then((Response r) {
                  if (r.json()['success'] == true) {
                    Phoenix.rebirth(context);
                  } else {
                    Scaffold.of(context).showSnackBar(
                        const SnackBar(content: Text('Login failed')));
                  }
                });
              }
            },
            child: Text(AppLocalizations.of(context).translate('SubmitButton')),
          ),
        ],
      ),
    );
  }
}
