import 'package:flutter/material.dart';
import 'package:garden_go/screens/settings.dart';
import 'package:garden_go/plants_list.dart';

import 'app_localizations.dart';

import 'not_logged_in_page.dart';
import 'profile.dart';

class LoggedInMainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Welcome back'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute<ProfilePage>(
                          builder: (BuildContext context) => ProfilePage()));
                },
                child: Text(AppLocalizations.of(context).translate('Profile')),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute<PlantsListPage>(
                          builder: (BuildContext context) => PlantsListPage()));
                },
                child: const Text('See Nearby Plants'),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute<NotLoggedInPage>(
                          builder: (BuildContext context) =>
                              NotLoggedInPage()));
                },
                child: const Text('Change User'),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.push<MaterialPageRoute>(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => SettingsPage()));
                },
                child: Text(AppLocalizations.of(context).translate('Settings')),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
