import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:garden_go/app_localizations.dart';
import 'package:requests/requests.dart';
import 'package:file_picker/file_picker.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool reloadPicture = true;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).translate('Profile')),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Container(
              child: Column(
                children: <Widget>[
                  Card(
                      child: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      FutureBuilder(
                          future: Requests.get(
                              'https://aqueous-tundra-22992.herokuapp.com/users/profile/'),
                          builder: (BuildContext context,
                              AsyncSnapshot<Response> snapshot) {
                            if (snapshot.hasData) {
                              final Response data = snapshot.data;
                              if (data.success) {
                                final json =
                                    data.json() as Map<String, dynamic>;
                                final String username =
                                    json['username'] as String;
                                final int points = json['points'] as int;
                                return ListTile(
                                  // leading: Icon(Icons.arrow_drop_down_circle),
                                  title: Center(
                                      child: Text(
                                    username,
                                    style: DefaultTextStyle.of(context)
                                        .style
                                        .apply(fontSizeFactor: 3.0),
                                  )),
                                  subtitle: Center(
                                      child: Text(
                                    '$points points',
                                    style: DefaultTextStyle.of(context)
                                        .style
                                        .apply(fontSizeFactor: 1.5),
                                  )),
                                );
                              }
                            }
                            return const CircularProgressIndicator();
                          }),
                      const SizedBox(
                        height: 10,
                      ),
                      FutureBuilder<Response>(
                          future: Requests.get(
                              'https://aqueous-tundra-22992.herokuapp.com/users/avatar/'),
                          builder: (BuildContext context,
                              AsyncSnapshot<Response> snapshot) {
                            if (snapshot.hasData) {
                              reloadPicture = false;
                              if (snapshot.data.success)
                                return Image.memory(
                                  snapshot.data.bytes() as Uint8List,
                                  width: 300,
                                  height: 400,
                                );
                              else
                                return const Icon(
                                  Icons.account_circle,
                                  size: 100,
                                  color: Colors.blue,
                                );
                              // return Text(snapshot.data.content());
                            } else if (snapshot.hasError) {}
                            return const CircularProgressIndicator();
                          }),
                      OutlineButton(
                        onPressed: () async {
                          final FilePickerResult result = await FilePicker
                              .platform
                              .pickFiles(type: FileType.image);
                          if (result != null) {
                            final String extension =
                                result.files.single.extension;
                            final File file = File(result.files.single.path);
                            final Uint8List bytes = file.readAsBytesSync();
                            final String base64Bytes = base64Encode(bytes);
                            final String base64Image =
                                'data:image/$extension;base64,$base64Bytes';
                            await Requests.post(
                                'https://aqueous-tundra-22992.herokuapp.com/users/avatar/',
                                body: <String, String>{
                                  'image': base64Image,
                                });
                            setState(() {
                              reloadPicture = true;
                            });
                          }
                        },
                        child: const Text('Choose Image'),
                      ),
                    ],
                  )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
