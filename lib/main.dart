import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:provider/provider.dart';
import 'package:requests/requests.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_localizations.dart';
import 'language_manager.dart';
import 'logged_in_main_page.dart';
import 'not_logged_in_page.dart';
import 'theme_manager.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  //prefs.clear();
  if (!prefs.containsKey('theme_preference')) {
    prefs.setInt('theme_preference', 1);
  }
  if (!prefs.containsKey('lang')) {
    prefs.setString('lang', 'en');
  }

  final LanguageManager appLanguage = LanguageManager();
  await appLanguage.fetchLocale();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp(appLanguage));
}

class MyApp extends StatelessWidget {
  const MyApp(this.appLanguage);

  final LanguageManager appLanguage;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeManager>(create: (_) => ThemeManager()),
        ChangeNotifierProvider<LanguageManager>(create: (_) => appLanguage),
      ],
      child: Consumer2<ThemeManager, LanguageManager>(
          builder: (context, themeManager, langManager, _) {
        return Phoenix(
            child: MaterialApp(
          title: 'Garden GO',
          theme: themeManager.themeData,
          locale: langManager.appLocal,
          supportedLocales: const [
            Locale('en'),
            Locale('fr'),
          ],
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          home: const MyHomePage(title: 'Garden GO Home Page'),
        ));
      }),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Response>(
        future: Requests.get(
            'https://aqueous-tundra-22992.herokuapp.com/users/test/'),
        builder: (BuildContext context, AsyncSnapshot<Response> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.success) {
              return LoggedInMainPage();
            } else {
              return NotLoggedInPage();
            }
          } else if (snapshot.hasError) {}
          return SafeArea(
              child: Scaffold(
                  appBar: AppBar(
                    title: const Text('Loading'),
                  ),
                  body: const Center(child: CircularProgressIndicator())));
        });
  }
}
