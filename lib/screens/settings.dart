import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import '../app_localizations.dart';
import '../app_theme.dart';
import '../language_manager.dart';
import '../preferences_service.dart';
import '../theme_manager.dart';
import '../widgets/screen_title.dart';

class SettingsPage extends StatefulWidget {
  static const routeName = '/settingsPage';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  var preferences = Preferences();

  Widget buildSectionTitle(String title, BuildContext context) {
    return Text(
      title,
      style: TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
          color: Theme.of(context).accentColor),
    );
  }

  @override
  Widget build(BuildContext context) {
    final meQu = MediaQuery.of(context);

    final PreferredSizeWidget appBar = (Platform.isIOS
        ? CupertinoNavigationBar(
            middle: Text(
              AppLocalizations.of(context).translate('Settings'),
            ),
            leading: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                BackButton(
                  color: Theme.of(context).accentColor,
                  onPressed: () => Navigator.pop(context),
                ),
              ],
            ),
          )
        : AppBar(
            leading: BackButton(
              color: Theme.of(context).accentColor,
              onPressed: () => Navigator.pop(context),
            ),
            backgroundColor: Theme.of(context).primaryColor,
            elevation: 0,
          )) as PreferredSizeWidget;

    final pageBody = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          //flex: 3,
          height: meQu.size.height * 0.2,
          child: ScreenTitle(
              text: AppLocalizations.of(context).translate('Settings')),
        ),
        //const SizedBox(height: 10.0),
        Container(
          //flex: 7,
          height: meQu.size.height * 0.68,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(20.0),
            child: FutureBuilder(
                future: preferences.getMySettings(),
                builder: (BuildContext context, AsyncSnapshot mySettings) {
                  if (mySettings.hasData) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        buildSectionTitle(
                            AppLocalizations.of(context).translate('Theme'),
                            context),
                        RadioListTile<dynamic>(
                          value: 0,
                          groupValue:
                              mySettings.data['theme_preference'] as int,
                          onChanged: (dynamic val) {
                            Provider.of<ThemeManager>(context, listen: false)
                                .setTheme(AppTheme.White);
                          },
                          title: Text(
                              AppLocalizations.of(context).translate('White')),
                          activeColor: Theme.of(context).primaryColor,
                        ),
                        RadioListTile<dynamic>(
                          value: 1,
                          groupValue:
                              mySettings.data['theme_preference'] as int,
                          onChanged: (dynamic val) {
                            Provider.of<ThemeManager>(context, listen: false)
                                .setTheme(AppTheme.Dark);
                          },
                          title: Text(
                              AppLocalizations.of(context).translate('Dark')),
                          activeColor: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        buildSectionTitle(
                            AppLocalizations.of(context).translate('Language'),
                            context),
                        RadioListTile<dynamic>(
                          value: 0,
                          groupValue: mySettings.data['lang'] as int,
                          onChanged: (dynamic val) {
                            Provider.of<LanguageManager>(context, listen: false)
                                .changeLanguage(const Locale('en'));
                          },
                          title: Text(AppLocalizations.of(context)
                              .translate('English')),
                          activeColor: Theme.of(context).primaryColor,
                        ),
                        RadioListTile<dynamic>(
                          value: 1,
                          groupValue: mySettings.data['lang'] as int,
                          onChanged: (dynamic val) {
                            Provider.of<LanguageManager>(context, listen: false)
                                .changeLanguage(const Locale('fr'));
                          },
                          title: Text(
                              AppLocalizations.of(context).translate('French')),
                          activeColor: Theme.of(context).primaryColor,
                        ),
                      ],
                    );
                  } else if (mySettings.hasError) {
                    return const Center(
                      child: Text('Error'),
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }),
          ),
        ),
      ],
    );

    return Scaffold(
      appBar: appBar,
      body: pageBody,
    );
  }
}
