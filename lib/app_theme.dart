import 'package:flutter/material.dart';

enum AppTheme { White, Dark }

String enumName(AppTheme anyEnum) {
  return anyEnum.toString().split('.')[1];
}

final appThemeData = {
  AppTheme.White: ThemeData(
    brightness: Brightness.light,
    backgroundColor: Colors.white,
    primarySwatch: Colors.green,
    primaryColor: Colors.green,
    accentColor: Colors.black,
    textTheme: ThemeData.light().textTheme.copyWith(
          headline6: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 45,
          ),
          subtitle2: const TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 20,
          ),
          headline4: const TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 20,
            color: Colors.green,
          ),
          button: const TextStyle(
            color: Colors.white,
            fontSize: 27,
            fontWeight: FontWeight.normal,
          ),
        ),
    appBarTheme: AppBarTheme(
      color: Colors.white,
      textTheme: ThemeData.light().textTheme.copyWith(
            headline6: const TextStyle(
              color: Colors.black87,
              fontSize: 35,
              fontWeight: FontWeight.bold,
            ),
          ),
    ),
  ),
  AppTheme.Dark: ThemeData(
    brightness: Brightness.dark,
    backgroundColor: Colors.black,
    primarySwatch: Colors.green,
    primaryColor: Colors.green,
    accentColor: Colors.white,
    textTheme: ThemeData.dark().textTheme.copyWith(
          headline6: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 45,
          ),
          subtitle2: const TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 20,
          ),
          headline4: const TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 20,
            color: Colors.green,
          ),
          button: const TextStyle(
            color: Colors.black,
            fontSize: 27,
            fontWeight: FontWeight.normal,
          ),
        ),
    appBarTheme: AppBarTheme(
      color: Colors.black,
      textTheme: ThemeData.dark().textTheme.copyWith(
            headline6: const TextStyle(
              color: Colors.white,
              fontSize: 35,
              fontWeight: FontWeight.bold,
            ),
          ),
    ),
  ),
};
