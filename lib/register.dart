import 'package:flutter/material.dart';
import 'package:garden_go/app_localizations.dart';
import 'package:requests/requests.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).translate('Register')),
        ),
        body: Center(
          child: RegisterForm(),
        ),
      ),
    );
  }
}

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController usernameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            validator: (String value) {
              if (value.isEmpty) {
                return 'Username field cannot be empty';
              }
              return null;
            },
            decoration: const InputDecoration(hintText: 'Username'),
            controller: usernameController,
          ),
          TextFormField(
            validator: (String value) {
              if (value.isEmpty) {
                return 'Email field cannot be empty';
              }
              return null;
            },
            decoration: const InputDecoration(hintText: 'Email'),
            controller: emailController,
          ),
          TextFormField(
            validator: (String value) {
              if (value.isEmpty) {
                return 'Password field cannot be empty';
              }
              return null;
            },
            decoration: const InputDecoration(hintText: 'Password'),
            obscureText: true,
            controller: passwordController,
          ),
          RaisedButton(
            onPressed: () {
              if (_formKey.currentState.validate()) {
                Scaffold.of(context).showSnackBar(
                    const SnackBar(content: Text('Connecting ...')));
                final Map<String, String> body = <String, String>{
                  'username': usernameController.text,
                  'email': emailController.text,
                  'password': passwordController.text,
                };
                Requests.post(
                        'https://aqueous-tundra-22992.herokuapp.com/users/register/',
                        body: body)
                    .then((Response r) {
                  if (r.json()['success'] == true) {
                    Navigator.pop(context);
                  } else {
                    Scaffold.of(context).showSnackBar(
                        const SnackBar(content: Text('Register failed')));
                  }
                });
              }
            },
            child: Text(AppLocalizations.of(context).translate('SubmitButton')),
          ),
        ],
      ),
    );
  }
}
