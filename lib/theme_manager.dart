import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app_theme.dart';

class ThemeManager with ChangeNotifier {
  ThemeManager() {
    _loadTheme();
  }

  ThemeData _themeData;

  final _kThemePreference = 'theme_preference';

  void _loadTheme() {
    SharedPreferences.getInstance().then((prefs) {
      final int preferredTheme = prefs.getInt(_kThemePreference) ?? 0;
      _themeData = appThemeData[AppTheme.values[preferredTheme]];
      // Once theme is loaded - notify listeners to update UI
      notifyListeners();
    });
  }

  /// Use this method on UI to get selected theme.
  ThemeData get themeData {
    _themeData ??= appThemeData[AppTheme.White];
    return _themeData;
  }

  /// Sets theme and notifies listeners about change.
  Future<void> setTheme(AppTheme theme) async {
    _themeData = appThemeData[theme];

    // Here we notify listeners that theme changed so UI have to be rebuild
    notifyListeners();

    // Save selected theme into SharedPreferences
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(_kThemePreference, AppTheme.values.indexOf(theme));
    return;
  }
}
