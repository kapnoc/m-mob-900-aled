import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:requests/requests.dart';

class PlantsListPage extends StatefulWidget {
  @override
  _PlantsListPageState createState() => _PlantsListPageState();
}

class _PlantsListPageState extends State<PlantsListPage> {
  bool refresh = true;
  bool canAddPlant = true;
  int points = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Nearby Plants'),
        ),
        body: Center(
          child: FutureBuilder(
            future: Geolocator.getLastKnownPosition(),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData) {
                final Position pos = snapshot.data as Position;
                final double lat = pos.latitude;
                final double lon = pos.longitude;
                return Column(
                  children: [
                    OutlineButton(
                      onPressed: () {
                        setState(() {
                          refresh = true;
                          canAddPlant = true;
                        });
                      },
                      child: const Text('Refresh'),
                    ),
                    FutureBuilder(
                        future: Requests.get(
                            'https://aqueous-tundra-22992.herokuapp.com/users/profile/'),
                        builder: (BuildContext context,
                            AsyncSnapshot<dynamic> snapshot) {
                          if (snapshot.hasData &&
                              snapshot.data.success == true) {
                            final Map<String, dynamic> data =
                                snapshot.data.json() as Map<String, dynamic>;
                            points = data['points'] as int;
                            return Text(
                              'Available points: ${data['points']}',
                              style: DefaultTextStyle.of(context)
                                  .style
                                  .apply(fontSizeFactor: 2.0),
                            );
                          }
                          return const CircularProgressIndicator();
                        }),
                    OutlineButton(
                      onPressed: canAddPlant && points > 1000
                          ? () async {
                              await Requests.post(
                                  'https://aqueous-tundra-22992.herokuapp.com/plants/',
                                  body: {
                                    'latitude': lat,
                                    'longitude': lon,
                                  });
                              setState(() {
                                refresh = true;
                                canAddPlant = true;
                              });
                            }
                          : null,
                      child: Column(
                        children: const [
                          Text('Add Plant to current location '),
                          Text('(1000 points)'),
                        ],
                      ),
                    ),
                    FutureBuilder(
                        future: Requests.get(
                            'https://aqueous-tundra-22992.herokuapp.com/plants/?lat=$lat&lon=$lon'),
                        builder: (BuildContext context,
                            AsyncSnapshot<dynamic> snapshot) {
                          if (snapshot.hasData &&
                              snapshot.data.success == true) {
                            final Map<String, dynamic> data =
                                snapshot.data.json() as Map<String, dynamic>;
                            final plants = data['plants'] as List<dynamic>;
                            return ListView.builder(
                                shrinkWrap: true,
                                itemCount: plants.length,
                                itemBuilder: (BuildContext context, int index) {
                                  final plant =
                                      plants[index] as Map<String, dynamic>;
                                  if (plant['distance'] as double < 25) {
                                    canAddPlant = false;
                                  }
                                  final directionAngle =
                                      (plant['direction'] as double) % 360;
                                  String direction = '';
                                  if (directionAngle <= 45 ||
                                      directionAngle >= 315) {
                                    direction = 'North';
                                  }
                                  if (directionAngle >= 45 &&
                                      directionAngle <= 135) {
                                    direction = 'East';
                                  }
                                  if (directionAngle >= 135 &&
                                      directionAngle <= 225) {
                                    direction = 'South';
                                  }
                                  if (directionAngle >= 225 &&
                                      directionAngle <= 315) {
                                    direction = 'West';
                                  }
                                  return Card(
                                    child: ListTile(
                                        leading: const Icon(Icons.arrow_right),
                                        title: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Text(
                                                  'n°${plant['id']}: ${plant['distance'].round()}m away $direction',
                                                  style: DefaultTextStyle.of(
                                                          context)
                                                      .style
                                                      .apply(
                                                          fontSizeFactor: 2.5),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Text(
                                                  'lvl: ${plant['level']} ; points: ${plant['points'].round()}',
                                                  style: DefaultTextStyle.of(
                                                          context)
                                                      .style
                                                      .apply(
                                                          fontSizeFactor: 2.5),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        subtitle: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: <Widget>[
                                              FlatButton(
                                                child: const Text('Harvest'),
                                                onPressed: plant['distance']
                                                            as double <
                                                        100
                                                    ? () async {
                                                        await Requests.get(
                                                            'https://aqueous-tundra-22992.herokuapp.com/plants/harvest/${plant['id'] as int}');
                                                        setState(() {
                                                          refresh = true;
                                                          canAddPlant = true;
                                                        });
                                                      }
                                                    : null,
                                              ),
                                              FlatButton(
                                                child: Text(
                                                    'Upgrade (${plant['level'] as int}k pts)'),
                                                onPressed: plant['distance']
                                                                as double <
                                                            100 &&
                                                        points >
                                                            1000 *
                                                                (plant['level']
                                                                    as int)
                                                    ? () async {
                                                        await Requests.get(
                                                            'https://aqueous-tundra-22992.herokuapp.com/plants/upgrade/${plant['id'] as int}');
                                                        setState(() {
                                                          refresh = true;
                                                          canAddPlant = true;
                                                        });
                                                      }
                                                    : null,
                                              ),
                                            ])),
                                  );
                                });
                          }
                          return const CircularProgressIndicator();
                        }),
                  ],
                );
              }
              return const CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.blue));
            },
          ),
        ),
      ),
    );
  }
}
