import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageManager extends ChangeNotifier {
  Locale _appLocale = const Locale('en');

  Locale get appLocal => _appLocale ?? const Locale('en');

  Future<void> fetchLocale() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getString('lang') == null) {
      _appLocale = const Locale('en');
      return;
    }
    _appLocale = Locale(prefs.getString('lang'));
    return;
  }

  Future<void> changeLanguage(Locale type) async {
    final prefs = await SharedPreferences.getInstance();
    if (_appLocale == type) {
      return;
    }
    if (type == const Locale('fr')) {
      _appLocale = const Locale('fr');
      await prefs.setString('lang', 'fr');
    } else {
      _appLocale = const Locale('en');
      await prefs.setString('lang', 'en');
    }
    notifyListeners();
    return;
  }
}
