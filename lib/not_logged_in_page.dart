import 'package:flutter/material.dart';
import 'package:garden_go/app_localizations.dart';

import 'package:garden_go/screens/settings.dart';

import 'login.dart';
import 'register.dart';

class NotLoggedInPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).translate('Welcome')),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute<RegisterPage>(
                          builder: (BuildContext context) => RegisterPage()));
                },
                child: Text(AppLocalizations.of(context).translate('Register')),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute<LoginPage>(
                          builder: (BuildContext context) => LoginPage()));
                },
                child: Text(AppLocalizations.of(context).translate('Login')),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.push<MaterialPageRoute>(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => SettingsPage()));
                },
                child: Text(AppLocalizations.of(context).translate('Settings')),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
