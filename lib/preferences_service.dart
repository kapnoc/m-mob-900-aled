import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  Future<void> deleteAll() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  Future<Map<String, String>> getMyPreferences() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    final String lang = prefs.getString('lang') ?? 'english';
    print('Shared Prefs: $lang');

    return {
      'lang': lang,
    };
  }

  Future<Map<String, dynamic>> getMySettings() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int themeIndex = prefs.getInt('theme_preference');
    final String lang = prefs.getString('lang');
    int relatedLang;
    switch (lang) {
      case 'en':
        relatedLang = 0;
        break;
      case 'fr':
        relatedLang = 1;
        break;
    }

    return <String, int>{
      'theme_preference': themeIndex,
      'lang': relatedLang,
    };
  }

  Future<int> getMyTheme() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    final int themeIndex = prefs.getInt('theme_preference') ?? 0;

    return themeIndex;
  }
}
