# garden_go

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


# Building

to build this app, you will need to have flutter and the android SDK installed.

to get dependencies, run `flutter pub get`

you should then simply be able to run `flutter build apk` for android ; and `flutter build ios` for iOS.

you can then simply install the app on your phone.

for development purposes however, it's easier to use Android Studio's or Visual Studio Code's hot reload, through their flutter plugins.

the API is deployed on heroku, hence the url found in the `Requests` calls.
